use rand::{distributions::{Distribution, Uniform}, Rng};
use rand_distr::StandardNormal;
use rulinalg::matrix::{BaseMatrix, Matrix};

use crate::neural_networks::NeuralNetwork;

/// Generates a new generation by performing truncation selection, where the top individuals
/// from the given population (which needs to be sorted by fitness) are mutated to create a new
/// population, which has the same size as the given population.
///
/// Use `sort_by_fitness()` to sort the population.
///
/// The weights are mutated by applying noise from the standard normal distribution, which is
/// multiplied by the given mutation power.
///
/// The new population also contains an unmodified copy of the best scoring individual from the 
/// given individual. This technique is called elitism. Since the fitness evaluation is noisy,
/// it's not guaranteed that the chosen elite represents the true elite, but it should be close
/// to it.
///
/// # Arguments
/// * `population` - A vector of neural networks (that are sorted by fitness)
/// * `mutation_power` - Specifies by how much the weights should be mutated
/// * `truncation_size` - Specifies the number of individuals to be selected as the parents
///                       of the new generation
///
/// # Examples
/// ```rust
/// let mut population: Vec<NeuralNetwork> = ...;
/// 
/// for individual in &population {
///     individual.set_fitness(...);
/// }
///
/// NeuralNetwork::sort_by_fitness(&mut population);
/// population = NeuralNetwork::get_next_generation(population, 0.002, 50);
/// ```
///
/// # Panics
/// Panics, if:
/// * the truncation size is 0,
/// * the truncation size is greater or equal to the population size.
pub fn get_next_generation(population: Vec<NeuralNetwork>, mutation_power: f32, truncation_size: usize) -> Vec<NeuralNetwork> {        
    if truncation_size == 0 {
        panic!["The truncation size cannot be 0!"];
    }
    
    if truncation_size >= population.len(){
        panic!["The truncation size needs to be smaller than the population size!"];
    }

    let mut new_population: Vec<NeuralNetwork> = Vec::with_capacity(population.len()); 

    // Create a uniform distribution
    let uniform = Uniform::from(0..truncation_size);
    let mut rng = rand::thread_rng();
    
    // Iterate over the population size (minus 1), generating new individuals
    for _ in 0..population.len()-1 {
        // Clone a parent
        let mut offspring = population[uniform.sample(&mut rng)].clone();
        offspring.remove_fitness();

        // Iterate over the weights vector, replacing the weight matrices with mutated ones
        for i in 0..offspring.weight_matrices().len() {
            let weights = &offspring.weight_matrices()[i];
            
            let mut data = weights.clone().into_vec();
            let rows = weights.rows();
            let columns = weights.cols();

            // Iterate over each weight value and add gaussian noise
            for j in 0..data.len() {
                let noise: f32 = rng.sample(StandardNormal);
                data[j] += mutation_power * noise;
            }

            // Replace the original weights with the mutated weights
            offspring.set_weight_matrix_at_index(Matrix::new(rows, columns, data), i);
        }

        // Add the new individual to the new population
        new_population.push(offspring);
    }

    // Add the best scoring individual to the new population (as the elite)
    new_population.push(population[0].clone());

    new_population
}
