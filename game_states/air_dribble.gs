{
    "game_speed": 5.0,
    "ball": {
        "location": {
            "x": 0.0,
            "y": 0.0,
            "z": 1000.0
        },
        "velocity": {
            "x": 0.0,
            "y": 0.0,
            "z": -10.0
        },
        "angular_velocity": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
        }
    },
    "players": [
        {
            "boost_amount": 100.0,
            "location": {
                "x": 0.0,
                "y": 0,
                "z": 850.0
            },
            "rotation": {
                "pitch": 1.57079632679,
                "yaw": 0.0,
                "roll": 0.0
            },
            "velocity": {
                "x": 0.0,
                "y": 0.0,
                "z": 0.0
            },
            "angular_velocity": {
                "x": 0.0,
                "y": 0.0,
                "z": 0.0
            }
        }
    ]
}