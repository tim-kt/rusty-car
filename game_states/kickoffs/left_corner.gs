{
    "game_speed": 5.0,
    "ball": {
        "location": {
            "x": 0.0,
            "y": 0.0,
            "z": 92.75
        },
        "velocity": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
        },
        "angular_velocity": {
            "x": 0.0,
            "y": 0.0,
            "z": 0.0
        }
    },
    "players": [
        {
            "boost_amount": 33.0,
            "location": {
                "x": 2048.0,
                "y": -2560.0,
                "z": 17.01
            },
            "rotation": {
                "pitch": 0.0,
                "yaw": 2.35619449019,
                "roll": 0.0
            },
            "velocity": {
                "x": 0.0,
                "y": 0.0,
                "z": 0.0
            },
            "angular_velocity": {
                "x": 0.0,
                "y": 0.0,
                "z": 0.0
            }
        }
    ]
}