use std::{clone::Clone, cmp::Reverse, fs, path::Path};
use rand::{distributions::Uniform, Rng};
use rulinalg::{io::csv::Writer, matrix::{Matrix, BaseMatrix}, vector::Vector};

/// Represents a neural network
#[derive(Debug)]
pub struct NeuralNetwork {
    /// Vector (from the rust standard library) of weight matrices (from rulinalg)
    weight_matrices: Vec<Matrix<f32>>,
    fitness: Option<i32>
}

impl NeuralNetwork {
    /// Returns a neural network with a given shape and the weights initialized with the Xavier
    /// initialization.
    /// 
    /// Note: The weights have one more column than there are neurons in the previous layer because
    /// those weights are the bias for the next neuron. This allows for a vectorized implementation
    /// of forward propagation, where a 1.0 is added as the first value of each neuron vector. The
    /// next layer of neurons can then be computed by multiplying the weight matrix with the vector
    /// of the current layer.
    /// 
    /// # Arguments
    /// * `shape` - The shape of the neural network, which means the size of each layer, that is
    ///             input, hidden and output layers
    /// 
    /// # Examples
    /// ```rust
    /// // Creates a neural network with 4 input neurons, two hidden layer with 8 neurons each, and
    /// // 4 output neurons with the weights initialized with the Xavier initialization and bias
    /// // weights set to 0.0
    /// let neural_network = NeuralNetwork::new(vec![4, 8, 8, 4]);
    /// ```
    ///
    /// # Panics
    /// Panics, if the length of the shape 0 or 1 (since that would not be an actual neural 
    /// network).
    pub fn new(shape: Vec<usize>) -> NeuralNetwork {
        if shape.len() < 2 {
            panic!["The length of the shape vector needs to be at least 2!"];
        }
        // Create a vector for the weight matrices
        let mut weight_matrices: Vec<Matrix<f32>> = Vec::with_capacity(shape.len() - 1);

        // Generate weights for each layer (with weights for an intercept term, serving as bias)
        for index in 1..shape.len() {       
            let rows = shape[index];
            // Add one additional column for the bias weights
            let columns = shape[index - 1] + 1;
            
            // More information on Xavier initialization and especially this value can be found at:
            // https://towardsdatascience.com/weight-initialization-in-neural-networks-a-journey-from-the-basics-to-kaiming-954fb9b47c79
            let epsilon = f32::sqrt(6.0) / f32::sqrt((shape[index] + shape[index -1]) as f32);

            // Get a vector containing the data for the weight matrix
            let mut data = Vector::rand_vec(rows * columns, epsilon);

            // Set the bias weights to 0
            for i in 0..rows {
                data[i * columns] = 0.0;
            }

            // Add the weight matrix to the weight matrices vector
            weight_matrices.push(Matrix::new(rows, columns, data));
        }

        NeuralNetwork {
            weight_matrices: weight_matrices,
            fitness: None
        }
    }

    /// Gets the neural network's weight matrices.
    pub fn weight_matrices(&self) -> &Vec<Matrix<f32>> {
        &self.weight_matrices
    }

    /// Sets the neural network's weight matrix at the given index.
    ///
    /// # Arguments
    /// * `weight_matrix` - the weight matrix to set
    /// * `index` - index of weight matrices vector to overwrite
    pub fn set_weight_matrix_at_index(&mut self, weight_matrix: Matrix<f32>, index: usize) {
        self.weight_matrices[index] = weight_matrix;
    }

    /// Sets the neural network's fitness.
    ///
    /// # Arguments
    /// * `fitness` - the fitness to set
    pub fn set_fitness(&mut self, fitness: i32) {
        self.fitness = Some(fitness);
    }

    /// Removes the neural network's fitness.
    pub fn remove_fitness(&mut self) {
        self.fitness = None;
    }

    /// Gets the neural network's fitness as a Some(i32) or None if the neural network doesn't have
    /// a fitness assigned yet.
    pub fn fitness(&self) -> Option<i32> {
        self.fitness
    }

    /// Performs forward propagation with the input vector, vectorized.
    /// 
    /// # Arguments
    /// * `input` - the input vector to feed the neural network (without an intercept term)
    /// 
    /// # Examples
    /// ```rust
    /// let neural_network = NeuralNetwork::new(vec![4, 8, 8, 5]);
    /// let input = Vector::new(vec![1.0, 2.0, 3.0, 4.0]);
    ///
    /// // The result is a Vector<f32> with 5 values
    /// let result = neural_network.forward_propagate(input);
    /// ```
    ///
    /// # Panics
    /// Panics, if the size of the input vector doesn't match the columns of the first weight
    /// matrix.
    pub fn forward_propagate(&self, input: Vector<f32>) -> Vector<f32> {
        if input.size() + 1 != self.weight_matrices[0].cols() {
            panic!["The size of the input vector doesn't match the columns of the first weight 
                    matrix!"];
        }

        let mut layer = input;

        for weights in &self.weight_matrices {
            // Add intercept term
            layer = Vector::add_intercept_term(layer.clone());

            // Multiply weight matrix with layer vector
            layer = weights * layer;

            // Apply sigmoid function
            layer = layer.apply(&NeuralNetwork::sigmoid);
        }

        layer
    }

    /// Saves the neural network's weights in csv files in a given directory. Will overwrite 
    /// files!
    ///
    /// # Arguments
    /// * `directory` - Specifies what directory to save the weights in (make sure you use an
    ///                 absolute path since this should be executed from RLBotGUI)
    ///
    /// # Examples
    /// ```rust
    /// let neural_network = NeuralNetwork::new(vec![4, 8, 8, 5]);
    /// neural_network.to_csv_files(Path::new("my-neural-network"));
    /// // The result is
    /// // .
    /// // ├── my-neural-network/
    /// // │   ├── 0-weights.csv  
    /// // │   ├── 1-weights.csv  
    /// // │   ├── 2-weights.csv  
    /// ```
    /// 
    /// # Panics
    /// Panics, if:
    /// * the given directory cannot be created,
    /// * a `rulinalg::io::csv::Writer` to a file cannot be created,
    /// * the data of a weight matrix cannot be saved to a file.
    #[allow(dead_code)]
    pub fn to_csv_files(&self, directory: &Path) {
        // TODO save neural networks as the seed for their initialization and any seeds that were
        // used when mutating the weights
        //
        // Useful information: https://docs.rs/rand/0.5.5/rand/trait.SeedableRng.html

        // Create the directory if it doesn't exist
        fs::create_dir_all(directory).unwrap();

        for index in 0..self.weight_matrices.len() {
            // Create a path with the directory and filename joined
            let path  = directory.join(format!("{}-weights.csv", index));

            // Create a mutable writer to save the weight matrix
            let mut writer = Writer::from_file(path).unwrap();

            // Write to matrix data to file
            self.weight_matrices[index].write_csv(&mut writer).unwrap();
        }
    }

    /// Reads weights from the given directory and returns a new neural network with the fitness
    /// set to None.
    ///
    /// # Arguments
    /// * `directory` - Specifies what directory the weights (as csv files) are in
    ///
    /// # Examples
    /// ```rust
    /// let neural_network = NeuralNetwork::from_csv_files(Path::new("my-neural-network"));
    /// ```
    /// 
    /// # Panics
    /// Panics, if:
    /// * the directory cannot be read,
    /// * a path to a file in the direcory cannot be created,
    /// * the file's contents cannot be read,
    /// * a value from the file cannot be converted to a float.
    #[allow(dead_code)]
    pub fn from_csv_files(directory: &Path) -> NeuralNetwork {
        let mut weight_matrices: Vec<Matrix<f32>> = Vec::new();

        for path in fs::read_dir(directory).unwrap() {

            // I'm not sure why this is necessary but I guess I'm iterating over DirEntrys which 
            // have a path method (returning the path I actually need), but I first need to unwrap 
            // the Result before I can access it
            let path = path.unwrap().path();

            // Skip non-csv files
            if !path.to_string_lossy().ends_with(".csv") {
                continue;
            }

            let mut rows = 0;
            let mut columns = 0;
            let mut vec: Vec<f32> = Vec::new();

            let content = fs::read_to_string(path).unwrap();

            for line in content.split("\n") {
                // Skip empty lines
                if line.is_empty() {
                    continue;
                }

                for value in line.split(",") {
                    // Only get values from non-empty values
                    if !value.is_empty() {
                        vec.push(value.parse().unwrap());
                    }
                }

                // After the first iteration the length of the vector with the matrix values is
                // equivalent to the matrix columns
                if columns == 0 {
                    columns = vec.len()
                }

                rows += 1;
            }

            let weights = Matrix::new(rows, columns, vec);

            weight_matrices.push(weights);
        }

        NeuralNetwork {
            weight_matrices: weight_matrices,
            fitness: None
        }
    }

    /// Sorts the given population.
    /// 
    /// # Arguments
    /// * `population` - A mutable reference to a vector containing neural networks
    /// 
    /// # Examples
    /// ```rust    
    /// let mut population: Vec<NeuralNetwork> = ...;
    /// 
    /// for individual in &population {
    ///     individual.set_fitness(...);
    /// }
    ///
    /// NeuralNetwork::sort_by_fitness(&mut population);
    /// // population is sorted now
    /// ```
    pub fn sort_by_fitness(population: &mut Vec<NeuralNetwork>){
        // Sort neural network by fitness (in descending order)
        population.sort_unstable_by_key(|n| Reverse(n.fitness.unwrap()));
    }

    /// Returns sigmoid(x) where sigmoid is defined as 1 / (1 + e^(-x)).
    ///
    /// # Arguments
    /// * `x` - The value to apply to the sigmoid function
    ///
    /// # Examples
    /// ```rust
    /// let y = sigmoid(1.0); // should be approximately 0.73106
    /// ```
    pub fn sigmoid(x: f32) -> f32 {
        1.0 / (1.0 + (-x).exp())
    }
}

impl Clone for NeuralNetwork {

    /// Returns a copy of the neural network
    fn clone(&self) -> Self {
        NeuralNetwork {
            weight_matrices: self.weight_matrices.clone(),
            fitness: self.fitness
        }
    }
}

/// Contains utility methods for matrices from the rulinalg crate containing float values
trait MatrixUtilities {
    fn rand_matrix (shape: (usize, usize), epsilon: f32) -> Matrix<f32>;
}

impl MatrixUtilities for Matrix<f32> {    

    /// Returns a `rulinalg::matrix::Matrix<f32>` with the given shape, initialized with random 
    /// values from -epsilon to +epsilon.
    /// 
    /// # Arguments
    /// * `shape` - The shape of the matrix as (rows, columns)
    /// * `epsilon` - Defines the range of random values for the matrix
    /// 
    /// # Examples
    /// ```rust
    /// // Creates 4x6 matrix with random values from -0.3 to +0.3
    /// let matrix = Matrix::rand_matrix((4, 6), 0.3);
    /// ```
    fn rand_matrix(shape: (usize, usize), epsilon: f32) -> Matrix<f32> {
        let rows = shape.0;
        let columns = shape.1;
    
        Matrix::new(rows, columns, Vector::rand_vec(rows * columns, epsilon))
    }
}

/// Contains utility methods for vectors from the standard library and the rulinalg crate with
/// float values
trait VectorUtilities {
    fn rand_vec(size: usize, epsilon: f32) -> Vec<f32>;
    fn rand_vector(size: usize, epsilon: f32) -> Vector<f32>;
    fn add_intercept_term(vector: Vector<f32>) -> Vector<f32>;
}

impl VectorUtilities for Vector<f32> {

    /// Returns a `Vec<f32>` (from the rust standard library) with the given size, initialized with
    /// random values from -epsilon to +epsilon.
    /// 
    /// # Arguments
    /// * `size` - The size of the vector
    /// * `epsilon` - Defines the range of random values for the vector
    /// 
    /// # Examples
    /// ```rust
    /// // Creates Vec of size 5 with random values from -0.3 to +0.3
    /// let vec = Vector::rand_vec(5, 0.3);
    /// ```
    fn rand_vec(size: usize, epsilon: f32) -> Vec<f32> {
        let values: Vec<f32> = rand::thread_rng().sample_iter(&Uniform::from(-epsilon..epsilon))
                                                .take(size)
                                                .collect();

        values
    }

    /// Returns a `rulinalg::vector::Vector<f32>` with the given size, initialized with random 
    /// values from -epsilon to +epsilon.
    /// 
    /// # Arguments
    /// * `size` - The size of the vector
    /// * `epsilon` - Defines the range of random values for the vector
    /// 
    /// # Examples
    /// ```rust
    /// // Creates Vector of size 5 with random values from -0.3 to +0.3
    /// let vector = Vector::rand_vector(5, 0.3);
    /// ```
    fn rand_vector(size: usize, epsilon: f32) -> Vector<f32> {
        Vector::new(Self::rand_vec(size, epsilon))
    }

    /// Returns the given vector with a single 1.0 inserted at the beginning of the vector (as a so
    /// called "intercept term").
    ///
    /// # Arguments
    /// * `vector` - The vector to add the intercept term to
    ///
    /// # Examples
    /// ```rust
    /// let vector = Vector::new(vec![4.4, 2.5, 7.5]);
    /// let result = Vector::add_intercept_term(vector); // = [1.0, 4.4, 2.5, 7.5]
    /// ```
    fn add_intercept_term(vector: Vector<f32>) -> Vector<f32> {
        // Create a Vec<f32> from the Vector<f32>
        let mut vec = vector.into_vec();

        // Create a Vec<f32> with a single value (1.0)
        let mut temp = vec![1.0];

        // Append vec to temp
        temp.append(&mut vec);

        // Return a Vector<f32>
        Vector::new(temp)
    }
}
