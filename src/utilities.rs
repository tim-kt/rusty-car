use std::{fs::File, path::Path};

/// Represents the configuration for a training sesseion
pub struct Configuration {
    population_size: usize,
    num_generations: usize,
    truncation_size: usize,
    mutation_power: f32
}

impl Configuration {
    /// Returns a configuration object from the given YAML configuration file.
    ///
    /// # Arguments
    /// * `config_path` - the path to a YAML configuration file
    ///
    /// # Examples
    /// ```rust
    /// let config = Configuration::new(Path::new("path/to/config.yaml"));
    /// ```
    ///
    /// # Panics
    /// Panics, if:
    /// * the configuration file does not exist
    /// * the configuration file could not be opened
    /// * the YAML could not be parsed
    /// * a field is in an invalid format
    /// * a value is invalid
    pub fn new(config_path: &Path) -> Configuration {
        // Get string representation of path for output
        let config_path_str = config_path.to_string_lossy();

        // Check if the file exists
        if !config_path.exists() {
            panic!("Configuration file {} doesn't exist!", config_path_str);
        }

        // Open the configuration file
        let file = match File::open(config_path) {
            Ok(var) => var,
            Err(err) => panic!("Couldn't open {}: {:?}", config_path_str, err)
        };

        // Parse the YAML
        let config: serde_yaml::Value = match serde_yaml::from_reader(file) {
            Ok(var) => var,
            Err(err) => panic!("Couldn't parse {}: {:?}", config_path_str, err)
        };

        // Parse population size
        let population_size = Configuration::parse_as_usize(&config, "population_size");

        // Check if the given number is valid
        if population_size < 1 {
            panic!("Invalid number specified for population size! (must be 1 or more)");
        }

        // Parse number of generations
        let num_generations = Configuration::parse_as_usize(&config, "num_generations");

        // Check if the given number is valid
        if num_generations < 1 {
            panic!("Invalid number specified for number of generations! (must be 1 or more)");
        }

        // Parse truncation size
        let truncation_size = Configuration::parse_as_usize(&config, "truncation_size");

        // Check if the given number is valid
        if truncation_size < 1 {
            panic!("Invalid number specified for population size! (must be 1 or more)");
        }

        if truncation_size > population_size {
            panic!("Invalid number specified for population size! (cannot be greater than population size)");
        }

        // Parse mutation power
        let mutation_power = Configuration::parse_as_f32(&config, "mutation_power");

        /*let sequence = match config["sequence"].as_sequence() {
            Some(var) => var,
            None => panic!("Invalid format of the training sequence! (must be a list of files)")
        };*/


        Configuration {
            population_size: population_size,
            num_generations: num_generations,
            truncation_size: truncation_size,
            mutation_power: mutation_power
        }
    }

    /// Parses the YAML field by the given name as a f32.
    ///
    /// # Arguments
    /// * `value` - a serde_yaml::Value object (representing the configuration)
    ///
    /// # Examples
    /// ```rust
    /// // Configuration YAML
    /// let config: serde_yaml::Value = ...;
    /// let mutation_power = Configuration::parse(config, "mutation_power");
    /// ```
    ///
    /// # Panics
    /// Panics, if the specified field cannot be parsed as f64.
    fn parse_as_f32(value: &serde_yaml::Value, name: &str) -> f32 {
        match value[name].as_f64() {
            Some(var) => var as f32,
            None => panic!("Invalid format of {}! (must be a number)", name)
        }
    }
    /// Parses the YAML field by the given name as a usize.
    ///
    /// # Arguments
    /// * `value` - a serde_yaml::Value object (representing the configuration)
    ///
    /// # Examples
    /// ```rust
    /// // Configuration YAML
    /// let config: serde_yaml::Value = ...;
    /// let population_size = Configuration::parse(config, "population_size");
    /// ```
    ///
    /// # Panics
    /// Panics, if the specified field cannot be parsed as f64.
    fn parse_as_usize(value: &serde_yaml::Value, name: &str) -> usize {
        Configuration::parse_as_f32(value, name) as usize
    }

    // Gets the population size.
    pub fn population_size(&self) -> usize {
        self.population_size
    }

    // Gets the number of generations.
    pub fn num_generations(&self) -> usize {
        self.num_generations
    }
    
    // Gets the truncation size.
    pub fn truncation_size(&self) -> usize {
        self.truncation_size
    }

    // Gets the mutation power.
    pub fn mutation_power(&self) -> f32 {
        self.mutation_power
    }
}