mod utilities;
mod neural_networks;
mod neuroevolution;

use std::{env, fs, fs::{OpenOptions, File}, f32::consts::PI, path::Path};
use rlbot::{BallInfo, DesiredBallState, DesiredCarState, DesiredGameInfoState, DesiredGameState, 
    DesiredPhysics, GameTickPacket, PlayerInfo, Vector3Partial, RotatorPartial};
use rulinalg::vector::Vector;
use csv::Writer;

use crate::utilities::Configuration;
use crate::neural_networks::NeuralNetwork;

// Indices for the bot (that are used when training)
const PLAYER_INDEX: usize =  0;
const TEAM_INDEX: usize =  0;

// TODO mode for playing with the neural network
fn main() {
    /* 
    You need to run this using RLBotGUI.

    Mutators that should be set when running this:
        - Match Length: Unlimited
        - Respawn time: Disable goal reset
        - (Boost amount: Unlimited)
        - Everything else: Default

    Extra settings:
        - Skip replays: On
        - Instant start: On
        - Enable Lockstep: Off
        - Enable Rendering: On
        - Enable state setting: On
        - Auto Save Replay: Decide for yourself
    */

    // Get the RUSTY_CAR environment variable
    let rusty_car_dir = match env::var("RUSTY_CAR") {
        Ok(var) => var,
        Err(_) => panic!("The RUSTY_CAR environment variable is not set!")
    };

    let rusty_car_path = Path::new(&rusty_car_dir);
    
    // Parse config.yaml file
    let config = Configuration::new(&rusty_car_path.join("config.yaml"));

    let statistics_file = &rusty_car_path.join("statistics.csv"); 

    // Create statistics file (or delete its content if it already exists)
    match File::create(&statistics_file) {
        Ok(_) => println!("[Rusty car] Created file {}", statistics_file.to_str().unwrap()),
        Err(err) => panic!("Couldn't create file {}: {:?}", statistics_file.to_str().unwrap(), err)
    }

    // Create a population vector and create the neural networks
    let mut population: Vec<NeuralNetwork> = Vec::with_capacity(config.population_size());
    
    // Holds the maximum fitness of all time (also across generations)
    let mut max_fitness = -1000000;

    for _ in 0..config.population_size() {
        population.push(NeuralNetwork::new(vec![22, 64, 64, 8]));
    }

    let rlbot = rlbot::init().unwrap();

    // Since RLBotGUI starts the game for us, we just need to wait
    rlbot.wait_for_match_start().unwrap();

    let mut packets = rlbot.packeteer();
    
    println!("[Rusty car] Starting training!");
    // Parse the training.yaml configuration file
    let game_states = DesiredGameState::get_sequence_from_config(&rusty_car_path.join("config.yaml"));

    for generation in 0..config.num_generations() {

        // Get the next generation, unless this is the first population
        if generation != 0 {
            population = neuroevolution::get_next_generation(population, config.mutation_power(), config.truncation_size());
        }

        // Holds the lowest distance of this generation
        let mut lowest_distance: [f32; 5] = [4608.0; 5];

        for i in 0..config.population_size() {

            // Draw generation and individual
            let mut group = rlbot.begin_render_group(0);
            let green = group.color_rgb(0, 255, 0);
            group.draw_string_2d((10.0, 10.0), (2, 2), format!("Generation {}/{} - Individual {}/{}", generation + 1, config.num_generations(), i + 1, config.population_size()), green);
            group.render().unwrap();

            let individual = &mut population[i];

            // Holds the individual's fitness
            let mut fitness = 0;

            // Iterate over game states
            for game_state_num in 0..game_states.len() {
                // Get one packet to:
                // - Wait in case the round is inactive
                // - Save the current score
                let mut packet = packets.next().unwrap();
                
                while !packet.game_info.is_round_active {
                    packet = packets.next().unwrap();
                }

                // Holds the score (to check if the individual scored)
                let score = packet.teams[TEAM_INDEX].score;

                // Set the game state
                rlbot.set_game_state(&game_states[game_state_num]).unwrap();

                // Counts how many frames have passed
                let mut frames = 0;
                
                // Let the individual play
                loop {
                    // Get the next packet
                    let mut packet = packets.next().unwrap();


                    // Draw current and max fitness
                    let mut group = rlbot.begin_render_group(1);
                    let blue = group.color_rgb(0, 255, 255);

                    group.draw_string_2d((10.0, 50.0), (2, 2), format!("Max fitness: {} - Current fitness: {}", max_fitness, fitness), blue);
                    group.render().unwrap();
                    
                    // TODO evaluation should also be configurable in external files
                    let distance = distance_to_ball(&packet);

                    // Stop evaluating after 120 ticks
                    if frames > 120 {
                        // If the bot had to score a goal, punish
                        // fitness -= 100;

                        // Add the individual's current boost amount (to discourage wasting boost)
                        // fitness += packet.players[PLAYER_INDEX].boost;

                        // Subtract the distance to the ball from fitness
                        fitness -= distance as i32;
                        break;
                    }

                    if packet.game_info.is_round_active && packet.ball.is_some()  {
                        // The bot loses 1 point every frame (to encourage faster plays)
                        fitness -= 1;
                        
                        let input = get_input(&mut packet, &individual);

                        rlbot.update_player_input(PLAYER_INDEX as i32, &input).unwrap();
                    }


                    if distance < lowest_distance[game_state_num] {
                        // If the distance is less than the lowest distance, break the loop
                        lowest_distance[game_state_num] = distance;

                        // Subtract the distance to the ball from fitness
                        fitness -= distance as i32;
                        
                        break;
                    }

                    if packet.teams[PLAYER_INDEX].score > score {
                        // If the bot scored, reward and break the loop
                        // fitness += 100;
        
                        // Add the individual's current boost amount (to discourage wasting boost)
                        // fitness += packet.players[PLAYER_INDEX].boost;
        
                        break;
                    }
        
                    frames += 1;
                }
                
            }

            individual.set_fitness(fitness);

            if fitness > max_fitness {
                max_fitness = fitness;
            }
        }
    
        // Sort population by fitness
        NeuralNetwork::sort_by_fitness(&mut population);

        // Save every neural network from the population and write the fitness to a csv file

        // Appends to the given file
        let file = OpenOptions::new().append(true).open(statistics_file).unwrap();
        let mut writer = Writer::from_writer(file);

        for i in 0..population.len() {
            writer.write_field(format!("{}", population[i].fitness().unwrap())).unwrap();
        }

        // Write a record terminator
        writer.write_record(None::<&[u8]>).unwrap();
    }

    // Save the individual of the last population with the highest fitness
    let mut best_individual = &population[0];

    for i in 1..population.len() {
        if population[i].fitness() > best_individual.fitness() {
            best_individual = &population[i];
        }
    }

    best_individual.to_csv_files(&rusty_car_path.join(format!("neural_networks/atba")));
}

/// Contains methods to read game states from files, directories and config files.
trait GameStateUtilities {
    fn read_from_file(path: &Path) -> DesiredGameState;
    fn read_from_directory(directory: &Path) -> Vec<DesiredGameState>;
    fn get_sequence_from_config(config_path: &Path) -> Vec<DesiredGameState>;
}

impl GameStateUtilities for DesiredGameState {
    /// Creates a DesiredGameState object from a .gs file.
    ///
    /// # Arguments
    /// * `path` - the path to the .gs file
    ///
    /// # Examples
    /// ```rust
    /// // Creates a game state from the "easy_score.gs" file
    /// let game_state = DesiredGameState::read_from_file(Path::new("easy_score.gs"));
    /// ```
    /// 
    /// # Panics
    /// Panics, if:
    /// * the file does not have the .gs extension
    /// * the file does not exist 
    /// * the file could not be opened
    /// * the JSON could not be parsed
    /// * the game state's game speed is greater than 5.0
    fn read_from_file(path: &Path) -> DesiredGameState {
        // Get string representation of path for output
        let path_str = path.to_string_lossy();


        // Check if the file has the .gs extension
        if !path_str.ends_with(".gs") {
            panic!("Game state files need to have the .gs extension!")
        }

        // Check if the file exists
        if !path.exists() {
            panic!("File {} doesn't exist!", path_str)
        }

        // Open the game state file
        let file = match File::open(path) {
            Ok(var) => var,
            Err(err) => panic!("Couldn't open {}: {:?}", path_str, err)
        };

        // Parse the JSON
        let value: serde_json::Value = match serde_yaml::from_reader(file) {
            Ok(var) => var,
            Err(err) => panic!("Couldn't parse {}: {:?}", path_str, err)
        };

        let mut game_state = DesiredGameState::new();

        // Get game speed
        let game_speed = value["game_speed"].as_f64().unwrap() as f32;

        if game_speed > 5.0 {
            panic!("The maximum supported game speed is 5.0 (currently set to {})!", game_speed)
        }

        let game_info_state = DesiredGameInfoState::new().game_speed(game_speed);

        game_state = game_state.game_info_state(game_info_state);

        // This can be refactored a lot by implementing the Deserialize trait for the
        // Vector3Partial and RotatorPartial struct, which would turn all those five liners into
        // one liners, but I haven't looked into that yet.
        //
        // TODO implement Deserialize for Vector3Partial (and RotatorPartial): 
        // https://serde.rs/impl-deserialize.html

        // Get ball physics
        let json_ball_location = &value["ball"]["location"];
        let ball_location = Vector3Partial::new()
            .x(json_ball_location["x"].as_f64().unwrap() as f32)
            .y(json_ball_location["y"].as_f64().unwrap() as f32)
            .z(json_ball_location["z"].as_f64().unwrap() as f32);

        let json_ball_velocity = &value["ball"]["velocity"];
        let ball_velocity = Vector3Partial::new()
            .x(json_ball_velocity["x"].as_f64().unwrap() as f32)
            .y(json_ball_velocity["y"].as_f64().unwrap() as f32)
            .z(json_ball_velocity["z"].as_f64().unwrap() as f32);

        let json_ball_angular_velocity = &value["ball"]["angular_velocity"];
        let ball_angular_velocity = Vector3Partial::new()
            .x(json_ball_angular_velocity["x"].as_f64().unwrap() as f32)
            .y(json_ball_angular_velocity["y"].as_f64().unwrap() as f32)
            .z(json_ball_angular_velocity["z"].as_f64().unwrap() as f32);

        let ball_physics = DesiredPhysics::new().location(ball_location)                                                
            .velocity(ball_velocity)
            .angular_velocity(ball_angular_velocity);

        let ball_state = DesiredBallState::new().physics(ball_physics);

        game_state = game_state.ball_state(ball_state);

        let players = value["players"].as_array().unwrap();

        for player_index in 0..players.len() {
            let player = &players[player_index];

            // Get player physics
            let json_car_location = &player["location"];
            let car_location = Vector3Partial::new()
                .x(json_car_location["x"].as_f64().unwrap() as f32)
                .y(json_car_location["y"].as_f64().unwrap() as f32)
                .z(json_car_location["z"].as_f64().unwrap() as f32);
                
            let json_car_rotation = &player["rotation"];
            let car_rotation = RotatorPartial::new()
                .pitch(json_car_rotation["pitch"].as_f64().unwrap() as f32)
                .yaw(json_car_rotation["yaw"].as_f64().unwrap() as f32)
                .roll(json_car_rotation["roll"].as_f64().unwrap() as f32);
  
            let json_car_velocity = &player["velocity"];
            let car_velocity = Vector3Partial::new()
                .x(json_car_velocity["x"].as_f64().unwrap() as f32)
                .y(json_car_velocity["y"].as_f64().unwrap() as f32)
                .z(json_car_velocity["z"].as_f64().unwrap() as f32);

            let json_car_angular_velocity = &player["angular_velocity"];
            let car_angular_velocity = Vector3Partial::new()
                .x(json_car_angular_velocity["x"].as_f64().unwrap() as f32)
                .y(json_car_angular_velocity["y"].as_f64().unwrap() as f32)
                .z(json_car_angular_velocity["z"].as_f64().unwrap() as f32);

            let car_physics = DesiredPhysics::new().location(car_location)
                .rotation(car_rotation)                                       
                .velocity(car_velocity)
                .angular_velocity(car_angular_velocity);

            let car_state = DesiredCarState::new().physics(car_physics)
                .boost_amount(player["boost_amount"].as_f64().unwrap() as f32);

            game_state = game_state.car_state(player_index, car_state);
        }

        game_state
    }

    /// Creates a vector of DesiredGameState objects from .gs files in a given directory.
    ///
    /// # Arguments
    /// * `path` - the path to the .gs file
    ///
    /// # Examples
    /// ```rust
    /// // Creates a game state from the "easy_score.gs" file
    /// let game_state = DesiredGameState::read_from_file(Path::new("easy_score.gs"));
    /// ```
    /// 
    /// # Panics
    /// Panics, if:
    /// * there is something wrong with a game state file
    fn read_from_directory(directory: &Path) -> Vec<DesiredGameState> {
        let mut game_states: Vec<DesiredGameState> = Vec::new();

        for path in fs::read_dir(directory).unwrap() {
            let path = path.unwrap().path();

            // Skip non-gs files            
            if !path.to_string_lossy().ends_with(".gs") {
                continue;
            }

            game_states.push(DesiredGameState::read_from_file(&path));
        }

        game_states
    }

    // TODO documentation
    // TODO move to config struct struct
    fn get_sequence_from_config(config_path: &Path) -> Vec<DesiredGameState> {
        if !config_path.exists() {
            panic!("Configuration file {} doesn't exist!", config_path.to_str().unwrap());
        }

        // Open the configuration file
        let file = match File::open(config_path) {
            Ok(var) => var,
            Err(err) => panic!("Couldn't open {}: {:?}", config_path.to_str().unwrap(), err)
        };

        // Parse the YAML
        let value: serde_yaml::Value = match serde_yaml::from_reader(file) {
            Ok(var) => var,
            Err(err) => panic!("Couldn't parse {}: {:?}", config_path.to_str().unwrap(), err)
        };

        // TODO check if "sequence" exists

        let sequence = match value["sequence"].as_sequence() {
            Some(var) => var,
            None => panic!("Invalid format of the training sequence!")
        };

        let mut game_states: Vec<DesiredGameState> = Vec::new();

        // Iterate over specified game state files and add them to the game_states vector
        for yaml_path in sequence.iter() {
            let path = match yaml_path.as_str() {
                Some(var) => var,
                None => panic!("Invalid format of the game state path!")
            };

            game_states.push(DesiredGameState::read_from_file(Path::new(&path)));
        }

        game_states
    }

}

/// Generates inputs for the bot by passing the packet's relevant data to the given neural network.
///
/// # Arguments
/// * `packet` - The current GameTickPacket
/// * `neural_network` - A neural network with 22 input neurons
/// 
/// # Examples
/// ```rust
/// let neural_network = NeuralNetwork::new(vec![22, 64, 64, 8], 0.3);
/// let mut packet = ...;
/// let input = get_input(&mut packet, &neural_network);
/// rlbot.update_player_input(player_index, &input)?;
/// ```
fn get_input(mut packet: &mut GameTickPacket, neural_network: &NeuralNetwork) -> rlbot::ControllerState {
    // Pass the current tick's data through the network to get the controls
    let controls = neural_network.forward_propagate(Vector::new(get_data(&mut packet)));

    // Since the neural network's output layer get's passed through the sigmoid function, every
    // value from throttle to roll is already in the correct range (-1 to 1). For jump, boost and
    // handbrake, the output value can be mapped to true and false by looking at it as a the neural
    // network's confidence to jump, boost etc. Every value greater than 0.0 will be treated as
    // true while every value below zero will be treated as false.
    //
    // use_item is always false because this bot is not made for Rumble.
    rlbot::ControllerState {
        throttle: controls[0],
        steer: controls[1],
        pitch: controls[2],
        yaw: controls[3],
        roll: controls[4],
        jump: controls[5] > 0.0,
        boost: controls[6] > 0.0,
        handbrake: controls[7] > 0.0,
        use_item: false
    }
}

/// Gets ball and player data from a GameTickPacket as a vector with normalized values
///
/// # Arguments
/// * `packet` - The packet to get data from
///
/// # Examples
/// ```rust
/// let mut packets = rlbot.packeteer();
/// let mut packet = packets.next();
/// let data = get_data(&mut packet);
/// ```
///
/// # Panics
/// Panics if the packet doesn't ball or player data.
fn get_data(packet: &mut GameTickPacket) -> Vec<f32>{
    // Get ball data
    let mut data = packet.ball.as_ref().unwrap().to_vec();

    // Append player data
    data.append(&mut packet.players[PLAYER_INDEX].to_vec());

    data
}

// TODO doc
fn distance_to_ball(packet: &GameTickPacket) -> f32 {

    // Get the ball's physics
    let ball = &packet.ball.as_ref().unwrap().physics;

    // Get the player's physics
    let player = &packet.players[PLAYER_INDEX].physics;

    f32::sqrt((player.location.x - ball.location.x).powi(2) +
              (player.location.y - ball.location.y).powi(2) +
              (player.location.z - ball.location.z).powi(2))
}

/// Holds constants used to normalize values from a GameTickPacket
struct Field;

impl Field {
    // Field specific constants
    const WIDTH: f32 = 4096.0;
    const LENGTH: f32 = 5120.0;
    const HEIGHT: f32 = 2044.0;

    const GOAL_DEPTH: f32 = 892.755;
}

// TODO to_vec() should not normalize values, but to_normalized_vec() should
// TODO turn BallInfoToVec to ToVec struct with generics? 

/// Contains to_vec(), which turns a BallInfo instance to a simple vector with normalized values
trait BallInfoToVec {
    // Ball specific constants
    const MAX_VELOCITY: f32 = 6000.0;
    const MAX_ANGULAR_VELOCITY: f32 = 6.0;
    
    fn to_vec(&self) -> Vec<f32>;
}

impl BallInfoToVec for BallInfo {
    /// Creates a vector from the BallInfo, containing location, velocity and angular velocity,
    /// all normalized to be either in the range of (-1, 1) or (0, 1).
    ///
    /// # Examples
    /// ```rust
    /// let mut packets = rlbot.packeteer();
    /// let packet = packets.next();
    /// let ball_data = packet.ball.as_ref().unwrap().to_vec();
    /// ```
    fn to_vec(&self) -> Vec<f32> {
        // Create a vector with the capacity 9 (for location, velocity and angular velocity values)
        let mut vec: Vec<f32> = Vec::with_capacity(9);

        // Get the ball's physics
        let ball = &self.physics;

        // Add location values
        vec.push(ball.location.x / Field::WIDTH);
        vec.push(ball.location.y / Field::LENGTH);
        vec.push(ball.location.z / Field::HEIGHT);
        
        // Add velocity values
        vec.push(ball.velocity.x / BallInfo::MAX_VELOCITY);
        vec.push(ball.velocity.y / BallInfo::MAX_VELOCITY);
        vec.push(ball.velocity.z / BallInfo::MAX_VELOCITY);

        // Add angular velocity values
        vec.push(ball.angular_velocity.x / BallInfo::MAX_ANGULAR_VELOCITY);
        vec.push(ball.angular_velocity.y / BallInfo::MAX_ANGULAR_VELOCITY);
        vec.push(ball.angular_velocity.z / BallInfo::MAX_ANGULAR_VELOCITY);

        vec
    }
}


/// Contains to_vec(), which turns a PlayerInfo instance to a simple vector with normalized values
trait PlayerInfoToVec {
    // Player specific constants
    const MAX_VELOCITY: f32 = 2300.0;
    const MAX_ANGULAR_VELOCITY: f32 = 5.5;
    const MAX_BOOST: f32 = 100.0;
    
    fn to_vec(&self) -> Vec<f32>;
}

impl PlayerInfoToVec for PlayerInfo {
    /// Creates a vector from the PlayerInfo, containing location, rotation, velocity, angular
    /// velocity and boost, all normalized to be either in the range of (-1, 1) or (0, 1).
    ///
    /// # Examples
    /// ```rust
    /// let mut packets = rlbot.packeteer();
    /// let mut packet = packets.next();
    /// let player_data = &mut packet.players[0].to_vec()
    /// ```
    fn to_vec(&self) -> Vec<f32> {
        // Create a vector with the capacity 9 (for location, velocity and angular velocity values)
        let mut vec: Vec<f32> = Vec::with_capacity(13);

        // Get the player's physics
        let player = &self.physics;

        // Add location values
        vec.push(player.location.x / Field::WIDTH);
        vec.push(player.location.y / Field::LENGTH + Field::GOAL_DEPTH);
        vec.push(player.location.z / Field::HEIGHT);
        
        // Add rotation values
        vec.push(player.rotation.pitch / PI);
        vec.push(player.rotation.yaw / PI);
        vec.push(player.rotation.roll / PI);

        // Add velocity values
        vec.push(player.velocity.x / PlayerInfo::MAX_VELOCITY);
        vec.push(player.velocity.y / PlayerInfo::MAX_VELOCITY);
        vec.push(player.velocity.z / PlayerInfo::MAX_VELOCITY);

        // Add angular velocity values
        vec.push(player.angular_velocity.x / PlayerInfo::MAX_ANGULAR_VELOCITY);
        vec.push(player.angular_velocity.y / PlayerInfo::MAX_ANGULAR_VELOCITY);
        vec.push(player.angular_velocity.z / PlayerInfo::MAX_ANGULAR_VELOCITY);

        // Add boost
        vec.push(self.boost as f32 / PlayerInfo::MAX_BOOST);

        vec
    }
}
